@extends('principal')

@section('encabezado')
	<h1>
	#{{$pokemon->id}} {{$pokemon->nombre}}
		<a href="{{url('/generarPDFpokemon')}}/{{$pokemon->id}}">
			<span class="glyphicon glyphicon-save-file" aria-hidden="true"></span>
		</a>
	</h1>	
@stop


@section('contenido')
<section id="portfolio-information" class="padding-top">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				@if($pokemon->id<=99)					
					<img src="{{asset ("img/0$pokemon->id.png")}}" class="img-responsive" alt="">
				@else					
					<img src="{{asset ("img/$pokemon->id.png")}}" class="img-responsive" alt="">
				@endif
					
			</div>
			<div class="col-sm-6">				
				<div class="project-info overflow">					
					<ul class="elements">
						<li><i class="fa fa-angle-right"></i>
						 	Tipo:
						 	{{$tipo1->nombre}}
							@if($tipo2 != null)
							\ {{$tipo2->nombre}}
							@endif
						</li>
						<li><i class="fa fa-angle-right"></i> Peso: {{$pokemon->peso}} Kg. </li>
						<li><i class="fa fa-angle-right"></i> Altura: {{$pokemon->altura}} M. </li>						
					</ul>
				</div>
				PC
				<div class="progress">					
                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{$pokemon->pc}}" aria-valuemin="0" aria-valuemax="{{$pokemon->max_pc}}" style="width: {{($pokemon->pc/$pokemon->max_pc)*100}}%" >{{$pokemon->pc}}
                    </div>
                </div>
                PS
				<div class="progress">					
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{$pokemon->ps}}" aria-valuemin="0" aria-valuemax="{{$pokemon->max_ps}}" style="width: {{($pokemon->ps/$pokemon->max_ps)*100}}%">
                        {{$pokemon->ps}}
                    </div>
                </div>


                <div class="row">
                	<div class="col-sm-6">		
                		<hr>
                		<div class="col-sm-3">
                			<img src="{{asset ("img/stardust.png")}}" width="20px">			
                			{{$polvo->cantidad}}
                			<br>
                			Polvos Estelares 
                		</div>
                		<div class="col-sm-3">
                			<img src="{{asset ("img/candy.png")}}" width="20px">			
                			{{$caramelos->cantidad}}
                			<br>
                			Caramelos 
                		</div>
                	</div>
                </div>                				
				
				<div class="live-preview">
					@if($polvo->cantidad>=$pokemon->polvo_psubir && $caramelos->cantidad>=$pokemon->caramelos_psubir &&($pokemon->pc<$pokemon->max_pc || $pokemon->ps<$pokemon->max_ps))			
						<a href="{{url('/darPoder')}}/{{$pokemon->id}}" class="btn btn-lg btn-primary">Mas Poder</a>
					@else
						<a href="{{url('/darPoder')}}/{{$pokemon->id}}" class="btn btn-lg btn-primary" disabled>Mas Poder</a>						
					@endif
					<img src="{{asset ("img/stardust.png")}}" width="20px">						
					{{$pokemon->polvo_psubir}}
					<img src="{{asset ("img/candy.png")}}" width="20px">			
					{{$pokemon->caramelos_psubir}}
										
				</div>
			</div>
		</div>
	</div>
</section>

	

@stop