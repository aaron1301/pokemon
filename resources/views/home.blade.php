@extends('principal')

@section('encabezado')
	
@stop

@section('contenido')	
	<section id="home-slider">
        <div class="container">
            <div class="row">
                <div class="main-slider">
                    <div class="slide-text">
                        <h1>Pokemon</h1>
                        <p>¡Venusaur, Charizard, Blastoise, Pikachu, y otros muchos Pokémon se han descubierto en el planeta Tierra!</p>
                        <p>Ahora es tu oportunidad para descubrir y capturar a los Pokémon a tu alrededor</p>
                        <p>En esta pagina encontraras información de los diferentes tipos de pokemon.</p>                        
                    </div>                                        
                    <img src="img/pokeball_logo.png" class="slider-sun" alt="slider image" >                   
                </div>                                
            </div>

        </div>
        
    </section>

@stop