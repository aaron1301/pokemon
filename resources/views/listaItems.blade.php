@extends('principal')

@section('encabezado')
<h1>Lista Pokemon</h1>	
@stop


@section('contenido')
<table class="table table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>Item</th>
			<th>Cantidad</th>			
		</tr>
	</thead>

	<tbody>
		@foreach($items as $i)
		<tr>
			<td>{{$i->id}}</td>
			<td>{{$i->nombre}}</td>
			<td>{{$i->cantidad}}</td>			
			<td>
				<form action="{{url('/obtenrItem')}}/{{$i->id}}" method="POST">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input name="cantidad" type="number">
					<input type="submit" value="Obtener" class="btn btn-primary">					
				</form>
				
			</td>
		</tr>
		@endforeach

	</tbody>
</table>


@stop