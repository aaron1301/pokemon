<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Pokemon</title>
    <link href="{{asset ("css/bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{asset ("css/font-awesome.min.css")}}" rel="stylesheet">
    <link href="{{asset ("css/animate.min.css")}}" rel="stylesheet"> 
    <link href="{{asset ("css/lightbox.css")}}" rel="stylesheet"> 
    <link href="{{asset ("css/main.css")}}" rel="stylesheet">
    <link href="{{asset ("css/responsive.css")}}" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="{{asset ("images/ico/favicon.ico")}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset ("images/ico/apple-touch-icon-144-precomposed.png")}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset ("images/ico/apple-touch-icon-114-precomposed.png")}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset ("images/ico/apple-touch-icon-72-precomposed.png")}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset ("images/ico/apple-touch-icon-57-precomposed.png")}}">
</head><!--/head-->

<body>
    <header id="header">        
        <div class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="{{url('/inicio')}}">
                        <h1><img src="{{asset ("img/pokemon.png")}}" alt="logo" width="250px"></h1>
                    </a>
                    
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="{{url('/inicio')}}">Home</a></li>
                        <li><a href="{{url('/consultarTodos')}}">Pokemon</a></li>
                        <li><a href="{{url('/consultarItems')}}">Items</a></li>
                    </ul>
                </div>                
            </div>
        </div>
    </header>
    <!--/#header-->
        
    <section>
        <div class="container">
            @yield('encabezado')
            <hr>
            @yield('contenido')
        </div>        
    </section>
    <!--/#home-slider-->

    

    <footer id="footer">
        <div class="container">
            <div class="row">                
                <div class="col-sm-12">
                    <div class="copyright-text text-center">
                        <p>Pokemon inc &copy; 2016</p>                        
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/#footer-->

    <script type="text/javascript" src="{{asset ("js/jquery.js")}}"></script>
    <script type="text/javascript" src="{{asset ("js/bootstrap.min.js")}}"></script>
    <script type="text/javascript" src="{{asset ("js/lightbox.min.js")}}"></script>
    <script type="text/javascript" src="{{asset ("js/wow.min.js")}}"></script>
    <script type="text/javascript" src="{{asset ("js/main.js")}}"></script>   
</body>
</html>
