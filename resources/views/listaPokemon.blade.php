@extends('principal')

@section('encabezado')
	<h1>Lista Pokemon</h1>
@stop

@section('contenido')
<section id="projects" class="padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="sidebar portfolio-sidebar">
                        <div class="sidebar-item categories">
                            <h3>Ver Por Tipo</h3>
                            <ul class="nav navbar-stacked">
                                <li><a href="{{url('/consultarTodos')}}">Todos<span class="pull-right"></span></a></li>
                                @foreach($tipos as $t)
                                	<li><a href="{{url('/consultarTipo')}}/{{$t->id}}">{{$t->nombre}}<span class="pull-right"></span></a></li>
                                @endforeach                                
                            </ul>
                        </div>                                               
                    </div>
                </div>

                <div class="col-md-9 col-sm-8">
                    <div class="row">
                    	@foreach($pokemon as $p)
                    	<div class="col-xs-6 col-sm-6 col-md-4 portfolio-item branded folio">                    		
                    		<div class="panel panel-default">
                    			<div class="panel-heading">
                    				<h3 class="panel-title">#{{$p->id}} {{$p->nombre}}</h3>                    				                				
                    			</div>
                    			<div class="panel-body">
                    				@if($p->id<=99)
                    				<img src="{{asset ("img/0$p->id.png")}}" width="225px">
                    				@else
                    				<img src="{{asset ("img/$p->id.png")}}" width="225px">
                    				@endif
                    				<br>
                    				Tipo: {{$p->tipo1}}
                    				@if($p->tipo2 != null)
                    					\ {{$p->tipo2}}
                    				@endif                   				
                    				<br>					
                    				<a href="{{url('/verPokemon')}}/{{$p->id}}" class="btn btn-primary btn-xs">Mas Informacion</a>                                        
                                    
                    			</div>
                    		</div>
                    	</div>
                    	@endforeach                    	                                             
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop