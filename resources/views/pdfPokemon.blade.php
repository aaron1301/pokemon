<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{$pokemon->nombre}}</title>
</head>
<body>
	<div>
		<div>
			<div>
				@if($pokemon->id<=99)					
					<img src="{{asset ("img/0$pokemon->id.png")}}">
				@else					
					<img src="{{asset ("img/$pokemon->id.png")}}">
				@endif					
			</div>
			<div >				
				<div>
				<h1>#{{$pokemon->id}} {{$pokemon->nombre}} </h1> 					
					<ul>
						<li>
						 	Tipo:
						 	{{$tipo1->nombre}}
							@if($tipo2 != null)
							\ {{$tipo2->nombre}}
							@endif
						</li>
						<li> Peso: {{$pokemon->peso}} Kg. </li>
						<li> Altura: {{$pokemon->altura}} M. </li>
						<li> PC:  {{$pokemon->pc}}</li>
						<li> PS:  {{$pokemon->pc}}</li>							
					</ul>
				</div>			
				
			</div>
		</div>
	</div>
</body>
</html>