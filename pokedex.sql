
create database pokedex;
	use pokedex;
	create table tipo(
		id int not null,
		nombre varchar(25) not null,
		primary key(id)
		);

	insert into tipo(id,nombre) values(1,"Normal");
	insert into tipo(id,nombre) values(2,"Fuego");
	insert into tipo(id,nombre) values(3,"Agua");
	insert into tipo(id,nombre) values(4,"Hierba");
	insert into tipo(id,nombre) values(5,"Electrico");
	insert into tipo(id,nombre) values(6,"Hielo");
	insert into tipo(id,nombre) values(7,"Peleador");
	insert into tipo(id,nombre) values(8,"Veneno");
	insert into tipo(id,nombre) values(9,"Tierra");
	insert into tipo(id,nombre) values(10,"Volador");
	insert into tipo(id,nombre) values(11,"Psiquico");
	insert into tipo(id,nombre) values(12,"Insecto");
	insert into tipo(id,nombre) values(13,"Roca");
	insert into tipo(id,nombre) values(14,"Fantasma");
	insert into tipo(id,nombre) values(15,"Dragon");
	insert into tipo(id,nombre) values(16,"Acero");
	insert into tipo(id,nombre) values(17,"Hada");

	create table pokemon(
		id int not null,
		nombre varchar(25) not null,
		tipo int not null,
		tipo2 int,
		peso float not null,
		altura float not null,
		pc int not null,
		ps int not null,
		polvo_psubir int not null,
		caramelos_psubir int not null,
		max_pc int not null,
		max_ps int not null,
		updated_at timestamp,
		primary key(id),
		foreign key(tipo) references tipo(id),
		foreign key(tipo2) references tipo(id) 
		);
	

	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(1,"Bulbasaur",4,8,6.9,0.7,15,15,264,1,951,77);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(2,"Ivysaur",4,8,13,1,17,15,214,2,1483,100);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(3,"Venusaur",4,8,100,2,16,16,279,2,2392,132);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(4,"Charmander",2,null,8.5,0.6,18,11,282,2,841,67);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(5,"Charmeleon",2,null,19,1.1,18,20,210,2,1411,97);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(6,"Charizard",2,10,90.5,1.7,20,15,283,2,2413,129);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(7,"Squirtle",3,null,9,0.5,17,10,259,3,891,75);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(8,"Wartortle",3,null,22.5,1,20,15,274,2,1435,99);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(9,"Blastoise",3,null,85.5,1.6,18,17,273,3,2355,130);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(10,"Caterpie",12,null,2.9,0.3,19,18,222,1,367,77);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(11,"Metapod",12,null,9.9,0.7,16,18,275,2,397,84);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(12,"Butterfree",12,10,32,1.1,16,13,297,2,1315,100);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(13,"Weedle",12,null,3.2,0.3,16,18,285,3,372,69);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(14,"Kakuna",12,null,10,0.6,16,12,202,1,405,77);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(15,"Beedrill",12,8,29.5,1,20,16,210,2,1301,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(16,"Pidgey",1,10,1.8,0.3,18,13,246,1,585,69);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(17,"Pidgeotto",1,10,30,1.1,17,13,266,2,1096,105);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(18,"Pidgeot",1,10,39.5,1.5,16,14,284,1,1923,137);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(19,"Rattata",1,null,3.5,0.3,18,14,270,2,493,53);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(20,"Raticate",1,null,18.5,0.7,20,12,265,2,1304,92);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(21,"Spearow",1,10,2,0.3,20,13,211,1,591,69);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(22,"Fearow",1,10,30.8,1.2,16,17,237,1,1592,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(23,"Ekans",8,null,6.9,2,17,13,280,3,718,61);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(24,"Arbok",8,null,65,3.5,17,17,200,2,1611,100);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(25,"Pikachu",5,null,6,0.4,15,14,215,3,777,61);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(26,"Raichu",5,null,30,0.8,19,16,289,1,1859,100);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(27,"Sandshrew",9,null,12,0.6,16,13,223,1,695,84);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(28,"Sandslash",9,null,25.9,1,18,13,225,1,1654,124);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(29,"Nidoran♀",8,null,7,0.4,16,18,214,1,768,92);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(30,"Nidorina",8,null,20,0.8,17,12,282,3,1267,116);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(31,"Nidoqueen",8,9,60,1.3,16,14,200,2,2301,148);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(32,"Nidoran♂",8,null,9,0.5,15,14,218,1,737,78);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(33,"Nidorino",8,null,19.5,0.9,16,18,207,1,1236,102);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(34,"Nidoking",8,9,62,1.4,19,18,247,2,2291,133);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(35,"Clefairy",17,null,7.5,0.6,16,15,211,2,1074,116);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(36,"Clefable",17,null,40,1.3,20,16,204,2,2217,156);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(37,"Vulpix",2,null,9.9,0.6,17,17,229,1,725,65);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(38,"Ninetales",2,null,19.9,1.1,16,15,234,3,2015,121);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(39,"Jigglypuff",1,17,5.5,0.5,15,20,224,1,796,187);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(40,"Wigglytuff",1,17,12,1,16,11,276,2,1997,227);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(41,"Zubat",8,10,7.5,0.8,16,12,291,1,550,69);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(42,"Golbat",8,10,55,1.6,19,13,230,1,1760,124);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(43,"Oddish",4,8,5.4,0.5,18,16,246,1,1023,77);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(44,"Gloom",4,8,8.6,0.8,17,14,279,1,1537,100);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(45,"Vileplume",4,8,18.6,1.2,15,12,226,1,2307,124);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(46,"Paras",12,4,5.4,0.3,16,11,293,3,804,61);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(47,"Parasect",12,4,29.5,1,16,11,286,2,1592,100);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(48,"Venonat",12,8,30,1,18,16,238,1,912,100);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(49,"Venomoth",12,8,12.5,1.5,17,11,249,2,1730,116);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(50,"Diglett",9,null,0.8,0.2,15,20,275,1,365,21);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(51,"Dugtrio",9,null,33.3,0.7,18,17,265,2,1038,61);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(52,"Meowth",1,null,4.2,0.4,15,15,260,3,656,69);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(53,"Persian",1,null,32,1,15,15,227,1,1483,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(54,"Psyduck",3,null,19.6,0.8,17,16,224,1,987,84);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(55,"Golduck",3,null,76.6,1.7,16,15,226,2,2206,132);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(56,"Mankey",7,null,28,0.5,16,15,232,3,769,69);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(57,"Primeape",7,null,32,1,16,13,221,2,1704,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(58,"Growlithe",2,null,19,0.7,16,14,222,2,1199,92);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(59,"Arcanine",2,null,155,1.9,20,19,255,2,2781,148);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(60,"Poliwag",3,null,12.4,0.6,19,17,265,3,693,69);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(61,"Poliwhirl",3,null,20,1,15,12,223,3,1206,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(62,"Poliwrath",3,7,54,1.3,20,16,205,3,2321,148);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(63,"Abra",11,null,19.5,0.9,16,19,275,2,508,45);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(64,"Kadabra",11,null,56.5,1.3,19,10,235,1,1005,69);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(65,"Alakazam",11,null,48,1.5,17,20,260,2,1654,92);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(66,"Machop",7,null,19.5,0.8,15,14,227,3,968,116);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(67,"Machoke",7,null,70.5,1.5,17,20,205,2,1606,132);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(68,"Machamp",7,null,130,1.6,19,12,294,3,2406,148);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(69,"Bellsprout",4,8,4,0.7,20,16,217,3,990,84);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(70,"Weepinbell",4,8,6.4,1,20,18,275,1,1567,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(71,"Victreebel",4,8,15.5,1.7,20,17,219,2,2342,132);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(72,"Tentacool",3,8,45.5,0.9,19,20,298,3,794,69);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(73,"Tentacruel",3,8,55,1.6,15,11,204,3,2046,132);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(74,"Geodude",13,9,20,0.4,18,15,238,2,742,69);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(75,"Graveler",13,9,105,1,16,18,259,1,1294,92);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(76,"Golem",13,9,300,1.4,16,20,223,2,2126,132);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(77,"Ponyta",2,null,30,1,19,14,235,3,1370,84);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(78,"Rapidash",2,null,95,1.7,20,11,289,2,2024,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(79,"Slowpoke",3,11,36,1.2,15,12,228,3,1089,148);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(80,"Slowbro",3,11,78.5,1.6,15,15,238,3,2409,156);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(81,"Magnemite",5,16,6,0.3,15,13,215,2,774,45);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(82,"Magneton",5,16,60,1,16,11,252,2,1715,84);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(83,"Farfetch'd",1,10,15,0.8,15,19,241,2,1133,88);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(84,"Doduo",1,10,39.2,1.4,15,14,229,1,746,61);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(85,"Dodrio",1,10,85.2,1.8,18,14,209,3,1677,100);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(86,"Seel",3,null,90,1.1,19,15,278,1,985,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(87,"Dewgong",3,6,120,1.7,16,17,293,3,1975,148);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(88,"Grimer",8,null,30,0.9,20,16,299,3,1152,132);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(89,"Muk",8,null,30,1.2,16,12,274,2,2414,171);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(90,"Shellder",3,null,4,0.3,15,15,219,2,715,53);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(91,"Cloyster",3,6,132.5,1.5,15,15,246,3,1879,84);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(92,"Gastly",14,8,0.1,1.3,15,18,289,3,696,53);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(93,"Haunter",14,8,0.1,1.6,17,14,228,1,1240,77);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(94,"Gengar",14,8,40.5,1.5,19,10,241,1,1907,100);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(95,"Onix",13,9,210,8.8,15,18,243,1,745,61);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(96,"Drowzee",11,null,32.4,1,15,15,285,2,955,100);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(97,"Hypno",11,null,75.6,1.6,16,19,285,3,2012,140);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(98,"Krabby",3,null,6.5,0.4,16,20,294,1,686,53);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(99,"Kingler",3,null,60,1.3,15,12,283,2,1663,92);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(100,"Voltorb",5,null,10.4,0.5,17,15,240,2,733,69);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(101,"Electrode",5,null,66.6,1.2,15,14,219,2,1496,100);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(102,"Exeggcute",4,11,2.5,0.4,16,19,279,3,978,100);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(103,"Exeggutor",4,11,120,2,19,13,262,1,2752,156);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(104,"Cubone",9,null,6.5,0.4,18,16,292,3,889,84);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(105,"Marowak",9,null,45,1,17,18,218,3,1505,100);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(106,"Hitmonlee",7,null,49.8,1.5,20,14,219,3,1349,84);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(107,"Hitmonchan",7,null,50.2,1.4,18,16,245,2,1370,84);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(108,"Lickitung",1,null,65.5,1.2,16,15,281,1,1477,148);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(109,"Koffing",8,null,1,0.6,18,11,230,2,1025,69);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(110,"Weezing",8,null,9.5,1.2,20,13,269,2,2073,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(111,"Rhyhorn",9,null,115,1,20,17,266,2,1055,132);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(112,"Rhydon",9,null,120,1.9,16,15,288,2,2068,171);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(113,"Chansey",1,null,34.6,1.1,19,13,251,1,549,401);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(114,"Tangela",4,null,35,1,19,19,263,2,1586,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(115,"Kangaskhan",1,null,80,2.2,18,18,234,3,1875,171);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(116,"Horsea",3,null,8,0.4,16,16,204,1,688,53);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(117,"Seadra",3,null,25,1.2,17,13,207,2,1559,92);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(118,"Goldeen",3,null,15,0.6,19,14,239,3,851,77);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(119,"Seaking",3,null,39,1.3,19,17,217,1,1877,132);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(120,"Staryu",3,null,34.5,0.8,18,11,213,1,821,53);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(121,"Starmie",3,11,80,1.1,20,13,213,1,2007,100);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(122,"Mr. Mime",11,17,54.5,1.3,19,18,218,1,1345,69);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(123,"Scyther",12,10,56,1.5,20,13,222,2,1905,116);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(124,"Jynx",6,11,40.6,1.4,15,10,237,1,1563,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(125,"Electabuzz",5,null,30,1.1,15,16,217,3,1947,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(126,"Magmar",2,null,44.5,1.3,19,11,255,1,2086,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(127,"Pinsir",12,null,55,1.5,18,19,262,2,1950,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(128,"Tauros",1,null,88.4,1.4,19,12,210,1,1686,124);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(129,"Magikarp",3,null,10,0.9,15,11,280,1,203,37);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(130,"Gyarados",3,10,235,6.5,18,17,241,2,2498,156);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(131,"Lapras",3,6,220,2.5,17,16,295,2,2777,211);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(132,"Ditto",1,null,4,0.3,19,16,267,3,809,81);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(133,"Eevee",1,null,6.5,0.3,15,18,271,1,957,92);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(134,"Vaporeon",3,null,29,1,19,12,235,3,2618,211);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(135,"Jolteon",5,null,24.5,0.8,16,16,286,3,1968,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(136,"Flareon",2,null,25,0.9,17,13,210,1,2448,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(137,"Porygon",1,null,36.5,0.8,20,10,244,3,1540,108);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(138,"Omanyte",13,3,7.5,0.4,17,13,294,3,992,61);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(139,"Omastar",13,3,35,1,17,17,221,2,2058,116);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(140,"Kabuto",13,3,11.5,0.5,19,20,257,3,975,53);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(141,"Kabutops",13,3,40.5,1.3,15,12,257,2,1957,100);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(142,"Aerodactyl",13,10,59,1.8,20,13,204,2,1994,132);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(143,"Snorlax",1,null,460,2.1,20,11,276,1,2901,258);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(144,"Articuno",6,10,55.4,1.7,20,15,299,1,2776,148);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(145,"Zapdos",5,10,52.6,1.6,20,12,289,3,2907,148);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(146,"Moltres",2,10,60,2,15,18,290,2,3028,148);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(147,"Dratini",15,null,3.3,1.8,18,16,274,3,867,70);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(148,"Dragonair",15,null,16.5,4,15,12,263,1,1593,102);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(149,"Dragonite",15,10,210,2.2,16,17,276,2,3280,149);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(150,"Mewtwo",11,null,122,2,18,15,234,2,3904,173);
	insert into pokemon(id,nombre,tipo,tipo2,peso,altura,pc,ps,polvo_psubir,caramelos_psubir,max_pc,max_ps) values(151,"Mew",11,null,4,0.4,18,10,300,1,3087,163);


	create table item(
		id int auto_increment not null,
		nombre varchar(30) not null,
		cantidad int not null,
		updated_at timestamp,
		primary key(id)
		);

	insert into item(nombre,cantidad) values("Polvos Estelares",200);
	insert into item(nombre,cantidad) values("Caramelos",3);	

