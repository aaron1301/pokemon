<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Http\Requests;

class itemController extends Controller
{
    public function consultar(){
    	$items=Item::all();
    	return view('listaItems',compact('items'));
    }

    public function obtener($id, Request $datos){
    	$item=Item::find($id);
    	$item->cantidad=$item->cantidad+$datos->input('cantidad');
    	$item->save();
    	return Redirect('/consultarItems');
    }
}
