<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pokemon;
use App\Tipo;
use App\Item;
use App\Http\Requests;
use DB;

class pokemonController extends Controller
{
    public function consultarTodos(){
    	$pokemon=Pokemon::select('pokemon.id','pokemon.nombre','t1.nombre as tipo1','t2.nombre as tipo2','peso', 'altura','pc','ps','polvo_psubir','caramelos_psubir','max_pc','max_ps')
        ->leftjoin('tipo as t1','t1.id','=','tipo')
        ->leftjoin('tipo as t2','t2.id','=','tipo2')        
        ->get();    	
    	$tipos=Tipo::all();
    	return view('listaPokemon',compact('pokemon','tipos'));
    }

    public function consultarTipo($id){
        $pokemon=Pokemon::select('pokemon.id','pokemon.nombre','t1.nombre as tipo1','t2.nombre as tipo2','peso', 'altura','pc','ps','polvo_psubir','caramelos_psubir','max_pc','max_ps')
        ->leftjoin('tipo as t1','t1.id','=','tipo')
        ->leftjoin('tipo as t2','t2.id','=','tipo2')                
        ->where('tipo',$id)
        ->orwhere('tipo2',$id)
        ->get();
    	$tipos=Tipo::all();
    	return view('listaPokemon',compact('pokemon','tipos'));
    }

    public function verPokemon($id){
    	$pokemon=Pokemon::find($id);        
        $tipo1=Tipo::find($pokemon->tipo);
        $tipo2=Tipo::find($pokemon->tipo2);        
    	$polvo=Item::find(1);
    	$caramelos=Item::find(2);    	    	  	
    	return view('verPokemon',compact('pokemon','tipo1','tipo2','polvo','caramelos'));
    }

    public function darPoder($id){
    	$pokemon=Pokemon::find($id);
        $polvo=Item::find(1);
        $caramelos=Item::find(2);                      
        if($pokemon->polvo_psubir<=$polvo->cantidad && $pokemon->caramelos_psubir<=$caramelos->cantidad){
            if($pokemon->pc<$pokemon->max_pc){
                if($pokemon->ps<$pokemon->max_ps){
                    $aumento_pc=$pokemon->caramelos_psubir*15;
                    $aumento_ps=$pokemon->caramelos_psubir*3;
                    
                    if($pokemon->pc+$aumento_pc<$pokemon->max_pc){
                        $pokemon->pc=$pokemon->pc+$aumento_pc;
                    }else{
                        $pokemon->pc=$pokemon->max_pc;
                    }

                    if($pokemon->ps+$aumento_ps<$pokemon->max_ps){
                        $pokemon->ps=$pokemon->ps+$aumento_ps;
                    }else{
                        $pokemon->ps=$pokemon->max_ps;
                    }
                }else{
                    $aumento_pc=$pokemon->caramelos_psubir*15;
                    if($pokemon->pc+$aumento_pc<$pokemon->max_pc){
                        $pokemon->pc=$pokemon->pc+$aumento_pc;
                    }else{
                        $pokemon->pc=$pokemon->max_pc;
                    }
                    

                }
                $polvo->cantidad=$polvo->cantidad - $pokemon->polvo_psubir;
                $polvo->save();
                $caramelos->cantidad=$caramelos->cantidad-$pokemon->caramelos_psubir;
                $caramelos->save();
                $pokemon->caramelos_psubir=$pokemon->caramelos_psubir+1;
                $pokemon->polvo_psubir=$pokemon->polvo_psubir+100;
                $pokemon->save();
            }elseif($pokemon->ps<$pokemon->max_ps){
                $aumento_ps=$pokemon->caramelos_psubir*3;
                if($pokemon->ps+$aumento_ps<$pokemon->max_ps){
                        $pokemon->ps=$pokemon->ps+$aumento_ps;
                    }else{
                        $pokemon->ps=$pokemon->max_ps;
                    }
                    $polvo->cantidad=$polvo->cantidad - $pokemon->polvo_psubir;
                    $polvo->save();
                    $caramelos->cantidad=$caramelos->cantidad-$pokemon->caramelos_psubir;
                    $caramelos->save();
                    $pokemon->caramelos_psubir=$pokemon->caramelos_psubir+1;
                    $pokemon->polvo_psubir=$pokemon->polvo_psubir+100;
                    $pokemon->save();
            }


        }
        return Redirect('/verPokemon/'.$id);
    }

    public function generarPDFpokemon($id){
        $pokemon=Pokemon::find($id);        
        $tipo1=Tipo::find($pokemon->tipo);
        $tipo2=Tipo::find($pokemon->tipo2);
        $vista=view('pdfPokemon', compact('pokemon','tipo1','tipo2'));
        $dompdf=\App::make('dompdf.wrapper');
        $dompdf->loadHTML($vista);
        return $dompdf->stream('Pokemon.pdf');
    }
    
}
