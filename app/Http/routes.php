<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/ejemplo', 'principalController@index');

Route::get('/inicio', 'principalController@inicio');

Route::get('/consultarTodos', 'pokemonController@consultarTodos');

Route::get('/consultarTipo/{id}', 'pokemonController@consultarTipo');

Route::get('/verPokemon/{id}','pokemonController@verpokemon');	

Route::get('/darPoder/{id}','pokemonController@darPoder');

Route::get('/generarPDFpokemon/{id}', 'pokemonController@generarPDFpokemon');

Route::get('/consultarItems/','itemController@consultar');

Route::post('/obtenrItem/{id}','itemController@obtener'); 

